<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Project_controller
 *
 * @author User
 */
class Project_controller extends CI_Controller {
    //put your code here
    public function index(){   
        $data['error']="";
        $this->load->view('project_view',$data);
    }
    public function addProject(){
        $config['upload_path'] ='./projects_file/'; 
        $config['allowed_types'] = 'doc|docx|pdf|jpg';
        $config['max_size'] = '100000000';
        $this->load->library('upload', $config);
       //$this->upload->initialize($config); //if use autoload
        if (!$this->upload->do_upload()){
         $data=array('error'=>$this->upload->display_errors());
         $this->load->view('project_view', $data);
        }else{
         $upload_data=$this->upload->data();
         $data['project_name']=$this->input->post('project_name');
         $data['project_file_name']=$upload_data['file_name'];
         $data['user_id']=14;
         $this->load->model('project_model');
         $this->project_model->addProject($data);         
         redirect('project_controller/display_projects');
        } 
    }
    
    
    public function display_projects(){
        $this->load->model('project_model');
        $data['projects']=$this->project_model->get_projects();
        $this->load->view('display_projects',$data);
    }
}
