<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author User
 */

class User_controller extends CI_Controller {
    //put your code here
  public function __construct() {
            parent::__construct();
            $this -> load -> model('user_model');
	 
	}  
    
    public function index(){
         $this->load->view('user_login');
    }
    
    
    public function load_register(){
        $data['districts'] = $this -> user_model ->get_district();            
        $this->load->view('register_view',$data);
    }
    public function wellcome_user(){
        $this->load->view('welcome_user_view');
    }


    public function register(){
        $data['name']=$this->input->post('name'); // here $data['name'] name is database tables name and post('name') is form input name
        $data['fathersName']=$this->input->post('fathersName');
        $data['dateOfBirth']=$this->input->post('date');
        $data['district']=$this->input->post('district');        
        $data['thana']=$this->input->post('thana');
        $userPhoto['upload_path']='./userPhoto/';
        $userPhoto['allowed_types']='doc|docx|pdf|jpg';
        $userPhoto['max-size']='1000000';
        $this->load->library('upload',$userPhoto);
        if (!$this->upload->do_upload()){
         $data=array('error'=>$this->upload->display_errors());
         $this->load->view('project_view', $data);
        }else{
         $uploadUserPhoto=$this->upload->data();
         //$data['project_name']=$this->input->post('projectName');
         $data['userPhoto']=$uploadUserPhoto['file_name'];
         //$data['user_id']=14;
         //$this->load->model('project_model');
         //$this->project_model->addProject($data);         
         //redirect('project_controller/display_projects');
        }
        $data['email']=$this->input->post('email');
        $data['phone']=$this->input->post('phone');
        $data['projectName']=$this->input->post('projectName');
        $data['projectCategory']=$this->input->post('projectCategory');
        
        $config['upload_path'] ='./projects_file/'; 
        $config['allowed_types'] = 'doc|docx|pdf|jpg|html';
        $config['max_size'] = '100000000';
        $this->load->library('upload', $config);
       //$this->upload->initialize($config); //if use autoload
        if (!$this->upload->do_upload()){
         $data=array('error'=>$this->upload->display_errors());
         $this->load->view('project_view', $data);
        }else{
         $upload_data=$this->upload->data();
         //$data['project_name']=$this->input->post('projectName');
         $data['projectFileName']=$upload_data['file_name'];
         //$data['user_id']=14;
         //$this->load->model('project_model');
         //$this->project_model->addProject($data);         
         //redirect('project_controller/display_projects');
        }
        
        $this->load->model('user_model'); //user_model.php is loaded from model
        $this->user_model->register($data); // call register method from user_model 
        redirect('user_controller/wellcome_user');
    }
    public function display_user(){
        $this->load->model("user_model");
        $data['users']=$this->user_model->get_user();
        $this->load->view('display_user',$data);
    }
    
    public function user_login(){
        $this->load->model('user_model');
        $check=$this->user_model->get_login_check($this->input->post('username')); // here 'username' is html name="username"
       if($check>0){  
           session_start();
           $_SESSION['username']=$this->input->post('username');
           $this->load->view('user_projects');
       }else{
           $this->load->view('user_login');
       }        
    }
    
    public function logout(){
        session_destroy();
        redirect('user_controller');
    }
    
    public function get_thana($district){            
            $this->load->model('user_model');
            //header('Content-Type: application/x-json; charset=utf-8'); //11
            //echo(json_encode($this->city_model->get_cities($country))); //11            
            $bar=$this->user_model->get_thana($district);            
            foreach($bar as $v){
                echo "<option>".$v->thana_name."</option>";
            }
	}
}?>