<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author User
 */

class Admin_controller extends CI_Controller {
    //put your code here
        
    //public function index(){
         //$this->load->view('admin_login');
    //}   
    public function index(){
        $data['message']="";
        $this->load->view("admin_login",$data);
    }
    public function admin_login(){
        $this->load->model('admin_model');
        $check=$this->admin_model->get_admin_check($this->input->post('adminname')); // here 'adminname' is html name="adminname"
       if($check>0){ 
            //$this->load->model('admin_model');
            $data['users']=$this->admin_model->get_user();
            session_start();
            $_SESSION['username']=$this->input->post('adminname');
            $this->load->view('display_user',$data);
       }else{
           
           $data["message"]="Incorret Username or Password";
           $this->load->view('admin_login',$data);
       }        
    }
    public function display_user_from_admin(){
        $this->load->model('admin_model');
        $data['users']=$this->admin_model->get_user();
        $this->load->view('display_user',$data);
    }

        public function logout(){    
        redirect('admin_controller/index');    
        session_destroy(); 
    }
    

}?>