<?php include_once('header.php');?>
<div class="container">
  <div class="col-md-3">
      
  </div>
    <div class="col-md-6 col-xs-12">
        <div class="panel-group">
        <div class="panel panel-info">
          <div class="panel-heading"><h4>Project Submit Form</h4></div>
          <div class="panel-body">
              <?php echo form_open_multipart("user_controller/register") ;?>
                  <div class="row">                      
                          <div class="col-md-3">
                              <label>Name :</label>
                          </div>
                          <div class="col-md-9">
                              <input type="text" class="form-control" required="true" name="name" placeholder="Your Name">
                          </div>                      
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                        <label>Father's Name :</label><br>
                        <input type="text" class="form-control" required="true" name="fathersName" placeholder="Your Father's Name"><br>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                        <label>Date of Birth :</label><br>
                        <input type="text" class="form-control datepicker" id="date"  name="date"><br>
                      </div>
                  </div>
                  <div class="row">                      
                          <div class="col-md-3">
                            <label>Select District:</label><br>
                          </div>
                          <div class="col-md-3">
                              <select class="form-control" name="district" id="district">
<!--                                <option value="" selected>Select district</option>-->
                                    <?php foreach ($districts as $district) { ?>
                                                    <option value="<?php echo $district->id?>"><?php echo $district->district_name?></option>
                                            <?php } ?>
                                    ?>
                            </select>
                          </div>
                          <div class="col-md-3">
                            <label>Select Thana:</label><br>
                          </div>
                          <div class="col-md-3">
                              <select class="form-control" name="thana"m id="thana">
                                  <option value="">Select Your Thana</option>
                                  <option value="1">Sarail</option>
                                  <option value="2">Dhanmondi</option>
                                  <option value="3">Mirpur</option>
                                  <option value="4">Badda</option>
                                  <option value="5">Jatrabari</option>
                                  <option value="6">Kasba</option>
                                  <option value="7">Nasirnagar</option>
                                  <option value="8">Nabinagar</option>
                              </select>
                          </div>                      
                  </div><br>
                  <div class="row">
                      
                          <div class="col-md-6">
                              <label>Photo identification file upload:</label>
                          </div>
                          <div class="col-md-6">
                              <input type="file" required="true" name="photoFile"><br>
                          </div>                      
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                        <label>Email :</label><br>
                        <input type="text" class="form-control" required="true" name="email" placeholder="Your Email">
                      </div>
                      <div class="col-md-6">
                          <label>Phone :</label><br>
                          <input type="text" class="form-control" required="true" name="phone" placeholder="Your Phone"><br>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12">
                          <form action="#" >                          
                            <label>Project Name</label>
                            <input type="text" class="form-control" required="true" name="projectName" placeholder="Enter Your Project Name Please"><br>
                            <select class="form-control" name="projectCategory">
                                <option value="">Select Your Project Category</option>
                                <option value="1">Logo</option>
                                <option value="2">Banner</option>
                                <option value="3">UI Layout</option>
                            </select><br>
                            <div class="row">
                                <div class="col-md-6 ">
                                    <label >Upload Your file Please  :</label><br>
                                    <input type="file" required="true" name="userfile"><br>
                                </div>
                                <div class="col-md-6">
                                    <label>Submit Your Account</label><br>
                                    <input type="submit" class="btn btn-info form-control" value="Submit">                      
                                    <a href="<?php echo site_url('user_controller/user_login') ?>">Have You Already An Account?</a>                  
                                </div>
                            </div>
                          </form>
                      </div>                  
                  </div>  <br>              
              </forn>
          </div>
        </div>
       </div>
    </div>
    <div class="col-md-3">
      
    </div>
</div>

</body>
</html>
