<!DOCTYPE html>
<html lang="en">
<head>
  <title>Devsteam Ltd</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css')?>">  
  <link rel="stylesheet" href="<?php echo base_url('css/datepicker.css')?>">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="<?php echo base_url('js/bootstrap.js') ?>"></script>
  <script src="<?php echo base_url('js/bootstrap-datepicker.js') ?>"></script>
  <script type="text/javascript">// <![CDATA[
	 $(document).ready(function(){
		 $('#district').change(function(){                     
			 $("#thana > option").remove();
			 var district_id = $('#district').val();
			 $.ajax({
				 type: "POST",
				 url: "http://localhost/Devsteam_Project/user_controller/get_thana/"+district_id,
				 success: function(thana){
//                                    $.each(thana,function(id,city){
//                                        //alert(thana);
//                                        var opt = $('<option />').val(id).text(city);                                                
//                                        $('#thana').append(opt);
//                                        for(x in thana){
//                                            //document.write(x+cities[x]+"<br>");
//                                        }
//                                    });

                                   $("#thana").html(thana);
				 }
			 });
		 });

		 $('#district').addClass('form-control');
		 $('#thana').addClass('form-control');
	 });
	 // ]]>
	</script>
  <script>      
      $(function(){
          $('.datepicker').datepicker();         
      });
      
  </script>
  
  
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
      <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">DevesTeam</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <?php if(!isset($_SESSION)){
                    ?><li class="active"><a href="<?php echo base_url('admin_controller/admin_login') ?>">Home logi</a></li>  <?php
                }elseif(isset($_SESSION)){
                    ?><li><a href="#">Home llll</a></li> 
                        
                        <?php 
                }?>
                
                <li class="active"><a href="#">About Us</a></li>
                <li class=""><a href="#">Contact Us</a></li>
            </ul>
           
            <ul class="nav navbar-nav navbar-right">
                <?php 
                if(!isset($_SESSION)&& $this->uri->segment(1)==="admin_controller"){
                    ?><li><a href="<?php echo site_url('Admin_controller/index'); ?>"><span class="glyphicon glyphicon-log-in "></span> Admin Login</a></li><?php 
                }elseif(isset($_SESSION)){                
                        if($this->uri->segment(1)==="admin_controller"){    ?>
                        <li><a href="<?php echo site_url('admin_controller/logout');?>"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($_SESSION['username']);?> | Logout</a></li>
                        <?php }else if($this->uri->segment(1)==="user_controller"){?>
                        <li><a href="<?php echo site_url('user_controller/logout');?>"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($_SESSION['username']);?> | Logout</a></li>   
                    <?php }
                }else{?>
                    <li><a href="<?php echo site_url('user_controller/load_register');?>"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="<?php echo site_url('user_controller/');?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <?php }?>
            </ul>
            
        </div>
    </div>
  </div>
</nav>
    <br><br><br>

   
 